#include "pwm.h"

int1 EA1 = 0, EP1 = 0, EA2 = 0, EP2 = 0;
int8 mode = 0, cont = 0;
int16 aux = 0.0;int16 VAP=0.0;
void leds();
#int_TIMER2
void  TIMER2_isr(void) 
{
   // Read ADC every 5ms. This will also make up for the 1 ms that we must wait after reading the ADC.
   VAP=read_adc();
   
   // If mode == 1 => Engine running according to the potenciometer => aux = ADC value so that
   // we can use it to display the speed % of the engine through the LEDs.
   if(mode == 1) { aux=VAP; }
   
   // Detected pressed buttons:
   if(cont == 0) // Initially, get the input values for both buttons
   {
      EA1 = input(pin_A1);
      EA2 = input(pin_A2);
   }
   cont++;
   // Wait 15ms and get the buttons' input values again.
   if(cont == 2)
   {
      EP1 = input(pin_A1);
      EP2 = input(pin_A2);
      cont = 0;
   }
   // If the value when cont was 0 is 1 and the value when cont 2 is 0, then a button has ben pressed.
   if((EA1 == 1)&&(EP1 == 0)) {mode = 1;} // Check for button START Engine
   if((EA2 == 1)&&(EP2 == 0) && (mode == 1)) {mode = 2;} // Check for button Progressive STOP. You may only get to mode 2 from mode 1
   
   if((mode == 2)&&(cont == 1))   // mode = 2 => Progressive STOP; cont = 1 => Every 10 ms
   {
      aux=aux-(VAP/100); // With the max ADC value, aux will decrease by 10.23 every 10ms
      if(aux <= 11)      // Hence, we can stop at 11, so that aux does not reach negative numbers.
      {
         aux = 0.0;
         mode = 0;
      }
      set_pwm1_duty(aux);
   }
}


void main()
{
   setup_adc_ports(AN0);
   setup_adc(ADC_CLOCK_DIV_2);
   setup_spi(SPI_SS_DISABLED);
   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_1);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DIV_BY_4,249,5);  // Overflow 1ms, Interrupt 5ms
   setup_ccp1(CCP_PWM);
   set_pwm1_duty(0);                // RESET => Emergency STOP
   enable_interrupts(INT_TIMER2);
   enable_interrupts(GLOBAL);

   set_adc_channel(0);
   delay_us(10);
   
   // Initial state for Mode Flags
   output_high(pin_C0); // STOP
   output_low(pin_C1);  // Engine START
   output_low(pin_C3);  // Progressive STOP
   
   // Initial state for engine's speed % display via LED's
   output_low(pin_C4);
   output_low(pin_C5);
   output_low(pin_C6);
   output_low(pin_C7);
   output_B(0);
   
   while(TRUE)
   {
      // Since the inital mode is 0, and the initial PWM duty cycle is also 0, 
      // the reset button will be the Emergency STOP button.
      if(mode == 0)              // Engine Stopped
      {
         output_high(pin_C0);    // STOP
         output_low(pin_C1);     // Engine START
         output_low(pin_C3);     // Progressive STOP
         aux = 0;
      }
      if(mode == 1)              // Engine Running / Speed according to potenciometer
      {
         output_low(pin_C0);     // STOP
         output_high(pin_C1);    // Engine START
         output_low(pin_C3);     // Progressive STOP
         set_pwm1_duty(VAP);     // Set the PWM's duty cycle based on the ADC reading.
         // With this configuration, the ADC and the PWM share the same resolution.
         // This means that when the voltage at AN0 is VREF+ (in this case, 5V), VAP will be 1023.
         // Setting pwm1_duty to 1023 results in a 100% duty cycle.
         // Therefore, we can directly set the duty cycle with the value read from the ADC.
      }
      if(mode == 2)
      {
         output_low(pin_C0);     // STOP
         output_low(pin_C1);     // Engine START
         output_high(pin_C3);    // Progressive STOP
      }
      // Display the engine speed by controlling LEDs, with the LED states determined by the value of 'aux'. 
      // The value of 'aux' is set differently based on the system's current mode:
      // - In mode 1, 'aux' takes its value from the ADC, representing the engine speed.
      // - In mode 0, 'aux' is set to 0.
      // - In mode 2, 'aux' decreases progressively over time.
      leds();
     }
}

void leds() 
{
  if(aux < 85.25)
  {
   output_low(pin_C4);
   output_low(pin_C5);
   output_low(pin_C6);
   output_low(pin_C7);
   output_B(0);
  } 
  if((aux >= 85.25) && (aux < 170.5))
  {
   output_high(pin_C4);
   output_low(pin_C5);
   output_low(pin_C6);
   output_low(pin_C7);
   output_B(0);
  }
  if((aux >= 170.5) && (aux < 255.75))
  {
   output_high(pin_C4);
   output_high(pin_C5);
   output_low(pin_C6);
   output_low(pin_C7);
   output_B(0);
  }
  if((aux >= 255.75) && (aux < 341))
  {
   output_high(pin_C4);
   output_high(pin_C5);
   output_high(pin_C6);
   output_low(pin_C7);
   output_B(0);
  }
  if( (aux>=341) && (aux < 426.25))
  {
   
   output_high(pin_C4);
   output_high(pin_C5);
   output_high(pin_C6);
   output_high(pin_C7);
   output_B(0);
  }
  if((aux >= 426.25) && (aux < 511.5))
  {
   output_high(pin_C4);
   output_high(pin_C5);
   output_high(pin_C6);
   output_high(pin_C7);
   output_B(1);
  }
  if((aux >= 511.5) && (aux < 596.75))
  {
   output_high(pin_C4);
   output_high(pin_C5);
   output_high(pin_C6);
   output_high(pin_C7);
   output_B(3);
  }
  if((aux >= 596.75) && (aux < 682))
  {
   output_high(pin_C4);
   output_high(pin_C5);
   output_high(pin_C6);
   output_high(pin_C7);
   output_B(7);
  }
  if((aux >= 682) && (aux < 767.25))
  {
   output_high(pin_C4);
   output_high(pin_C5);
   output_high(pin_C6);
   output_high(pin_C7);
   output_B(15);
  }
  if((aux >= 767.25) && (aux < 852.5))
  {
   output_high(pin_C4);
   output_high(pin_C5);
   output_high(pin_C6);
   output_high(pin_C7);
   output_B(31);
  }
  if((aux >= 852.5) && (aux < 937.75))
  {
   output_high(pin_C4);
   output_high(pin_C5);
   output_high(pin_C6);
   output_high(pin_C7);
   output_B(63);
  }
  if((aux >= 937.75) && (aux < 1023))
  {
   output_high(pin_C4);
   output_high(pin_C5);
   output_high(pin_C6);
   output_high(pin_C7);
   output_B(127);
  }
  if(aux >= 1023)
  {
   output_high(pin_C4);
   output_high(pin_C5);
   output_high(pin_C6);
   output_high(pin_C7);
   output_B(255);
  }
}
