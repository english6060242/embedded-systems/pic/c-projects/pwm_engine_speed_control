# Pulse Width Modulation (PWM)

Pulse Width Modulation (PWM) is a modulation technique commonly used in electronic control systems to control the power delivered to electrical devices such as motors, LEDs, and heaters. In PWM, the average power delivered to the load is varied by varying the width of the pulse in a periodic signal, typically a square wave.

### How PWM Works:

- **Duty Cycle:** PWM operates by varying the duty cycle, which is the ratio of the pulse width (on-time) to the period of the signal. The duty cycle determines the average voltage or power delivered to the load. A higher duty cycle corresponds to a higher average voltage or power, while a lower duty cycle corresponds to a lower average voltage or power.

- **Frequency:** PWM signals have a fixed frequency, which is the inverse of the period. The frequency determines how often the PWM signal repeats within a given time period.

![PWM](img/pwm.png)

# PWM Engine Speed Control Project

![Schematic](img/Schematic.png)

This project is a part of the Digital Electronic Circuits and Measurements course. 

## Project Task
**Topic:** Pulse Width Modulation  

Develop a medium complexity project that applies the theoretical-practical knowledge acquired in digital electronic circuits, measurements, and electronic informatics by the students. The project employs pulse width modulation technique to control the speed of a DC motor.

## Project Description
- **Objective:** Control the speed of a 12V DC motor using a PIC microcontroller via pulse width modulation.
- **Hardware and Software:** Develop necessary hardware and software to control the motor speed.
- **Components:**
  - PIC Microcontroller: Recommended PIC16F873.
  - MOSFET Interface for motor control.
  - Voltage Regulator: 7805 for microcontroller power supply.
- **Functionality:**
  - Motor speed increases progressively with the increase in the voltage of a command potentiometer.
  - Motor speed is indicated by a 12 LED digital VU meter with 3 colors.
  - System includes 3 command buttons: Start, Progressive Brake, and Emergency Brake .
- **Design Details:**
  - Use 5.08mm pitch terminal blocks for connections.
  - Layout components symmetrically and neatly on the single-sided PCB.
  - Ensure clear labeling of components.
  - Use KiCad for schematic and PCB design.
- **Requirements:**
  - Submit clear and neat schematics and PCB layouts.
  - Include a well-commented and explained microcontroller software.

## Clone Repository

```bash
git remote add origin https://gitlab.com/english6060242/embedded-systems/pic/c-projects/pwm_engine_speed_control.git
git branch -M main
git pull origin main
